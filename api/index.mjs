import express from 'express'
import multer from 'multer'

const app = express()
const API_KEY = process.env.API_KEY
const API_KEY_READ = process.env.API_KEY_READ
const API_KEY_NOPARA = process.env.API_KEY_NOPARA


import {DB,CalendarHandler} from './module.mjs'

const db = new DB()
const ch = new CalendarHandler()

app.use(express.urlencoded({extended: true}))
app.use(/.*/, (req, res, next)=>{
    const READ_ONLY_PATH = ['/syncUrl', '/getPeriod']
    const EXCEPT_PATH = '/noPara'

    var authed = req.headers['authorization'] === API_KEY //full access
    authed ||= READ_ONLY_PATH.includes(req.baseUrl) && req.query.pw === API_KEY_READ //read only
    authed ||= req.baseUrl.startsWith(EXCEPT_PATH)

    if(!authed){
        res.status(401).send('unauthorized')
        return
    }

    next()
})

app.post('/putEvent', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()

    ch.load(db.kv)
    const {calName,start,end,summary,description} = req.body
    ch.add_event(calName, {start,end,summary,description})

    db.kv = ch.dump()
    await db.push_cache()
    await db.end_db()

    res.send(true)
})
app.post('/patchEvent', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()

    ch.load(db.kv)
    const {calName,index,start,end,summary,description} = req.body
    ch.patch_event(calName, index, {start,end,summary,description})

    db.kv = ch.dump()
    await db.push_cache()
    await db.end_db()

    res.send(true)
})
app.get('/deleteEvent', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()

    ch.load(db.kv)
    var {calName,indices} = req.query
    indices = indices.split(' ').map(
        n=>Number.parseInt(n)
    ).filter(
        n=>!Number.isNaN(n)
    )
    ch.delete_events(calName, indices)

    db.kv = ch.dump()
    await db.push_cache()
    await db.end_db()

    res.send(true)
})

app.get('/syncUrl', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()

    ch.load(db.kv)
    const calNames = req.query.calNames?
        [req.query.calNames].flat() : []
    const updated_cals = await ch.pull_url(calNames)

    if(Object.keys(updated_cals).length){
        db.kv = ch.dump()
        await db.push_cache()
    }
    await db.end_db()

    res.json(updated_cals)
})

//arr
app.get('/getPeriod', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()

    ch.load(db.kv)
    var results
    req.query.calNames = req.query.calNames?
        req.query.calNames.split(' ') : []

    res.json(ch.get_period(req.query))
})

app.get('/deleteAll', async (req,res)=>{
    await db.start_db()
    await db.purge_cache()
    await db.end_db()

    res.send(true)
})

app.get('/listCal', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()

    ch.load(db.kv)

    res.json(ch.list(req.query.isTstrg))
})
app.get('/createCal', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()

    ch.load(db.kv)
    const calNames = req.query.calNames.split(' ')
    ch.create_cals(calNames)

    db.kv = ch.dump()
    await db.push_cache()
    await db.end_db()

    res.send(true)
})
app.get('/deleteCal', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()

    ch.load(db.kv)
    const calNames = req.query.calNames.split(' ')
    ch.delete_cals(calNames)

    db.kv = ch.dump()
    await db.push_cache()
    await db.end_db()

    res.send(true)
})

app.get('/exportAll/json', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()

    res.set('Content-disposition', 'attachment; filename=calendar.json')
    res.json(db.kv)
})
//obj
app.get('/export/json', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()

    const calNames = req.query.calNames.split(' ')
    const fname = calNames.length===1? calNames[0] : 'calendar'
    ch.load(db.kv)

    res.set('Content-disposition', `attachment; filename=${fname}.json`)
    res.json(ch.dump_cals(calNames))
})
app.get('/exportAll/ical', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()

    ch.load(db.kv)
    const pack = await ch.exports(req.query.calNames)

    res.set('Content-Type', 'application/x-tar')
    res.set('Content-disposition', 'attachment; filename=calendar.tar')
    pack.pipe(res)
})
app.get('/export/ical', async (req,res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()

    const calNames = req.query.calNames.split(' ')
    ch.load(db.kv)

    if(calNames.length === 1){
        res.set('Content-Type', 'application/octet-stream')
        res.set('Content-disposition', `attachment; filename=${calName}.ics`)
        res.send(ch.export(calName))
    }else{
        res.set('Content-Type', 'application/x-tar')
        res.set('Content-disposition', 'attachment; filename=calendar.tar')
        const pack = await ch.exports(calNames)
        pack.pipe(res)
    }

})
app.get('/noPara/export/:api_key/:calName', async (req,res)=>{
    if(req.params.api_key !== API_KEY_NOPARA){
        res.status(401).send('unauthorized')
        return
    }

    await db.start_db()
    await db.pull_cache()
    await db.end_db()

    ch.load(db.kv)
    res.set('Content-Type', 'text/calendar')
    res.send(await ch.export(req.params.calName))
})

//overwrite
app.post('/load/json', multer().single('f'), async (req,res)=>{
    await db.start_db()
    db.kv = JSON.parse(req.file.buffer.toString())
    await db.push_cache()
    await db.end_db()
    res.send(true)
})

//not overwrite, just append
app.post('/import/:source', multer().any(), async (req,res)=>{
    await db.start_db()
    await db.pull_cache()

    ch.load(db.kv)
    switch(req.params.source){
        case 'json':
        case 'ical':
            for(const {fieldname, buffer} of req.files)
                await ch.import(fieldname, {[req.params.source]: buffer.toString()})
            break
        case 'url':{
            for(var [fieldname,urls] of Object.entries(req.body)){
                urls = [urls].flat() //single elemnt to array
                for(const url of urls)
                    await ch.import(fieldname, {[req.params.source]: url})
            }
            break
        }

    }

    db.kv = ch.dump()
    await db.push_cache()
    await db.end_db()
    res.send(true)
})

app.listen(process.env.PORT || 8080, () => console.log('Server ready'))