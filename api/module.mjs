import mysql from 'mysql2/promise'
import tar from 'tar-stream'
import icalParser from 'ical'
import icalGenerator from 'ical-generator'
import fetch from 'node-fetch'

const DB_PW_URI = process.env.DB_PW_URI
process.env.TZ = 'Asia/Hong_Kong'

class DB{
    static DEFAULT_KV_VALUE
    async start_db(){
        this.pool = mysql.createPool(DB_PW_URI)
        this.connection = await this.pool.getConnection()

        var [results, fields] = await this.connection.query(
            'CREATE TABLE IF NOT EXISTS `kv-table` ( \
                `key` VARCHAR(200) NOT NULL, \
                `value` MEDIUMTEXT, \
                PRIMARY KEY (`key`) \
            )'
        )
    }
    async end_db(){
        await this.connection.release()
        await this.pool.end()
    }
    //this.kv
    async pull_cache(force=false){
        if(this.kv && !force)
            return

        try{
            var [results, fields] = await this.connection.query(
                "SELECT * FROM `kv-table` WHERE `key`='calendar-kv'"
            )
            this.kv = JSON.parse(results[0]['value']) || this.DEFAULT_KV_VALUE
            //console.log(this.kv)
        }catch(e){
            //console.log(e)
            this.kv = this.DEFAULT_KV_VALUE
        }
    }
    async push_cache(){
        var [results, fields] = await this.connection.query(
            {sql:"REPLACE INTO `kv-table` (`key`,`value`) VALUES('calendar-kv',?)"},
            [JSON.stringify(this.kv)]
        )
    }
    async purge_cache(){
        var [results, fields] = await this.connection.query(
            "DELETE FROM `kv-table` WHERE `key`='calendar-kv'"
        )
        this.kv = this.DEFAULT_KV_VALUE
    }
}


class CalendarHandler{
    load(value){
        if(!value){
            this.cals = {}
            return
        }

        const {cals} = value
        this.cals = Object.fromEntries(Object.entries(cals).map(([calName,obj])=>
            [calName, new Calendar(obj)]
        ))
    }
    dump(){
        const cals = Object.fromEntries(Object.entries(this.cals).map(([calName,obj])=>(
            [calName, obj.dump()]
        )))
        return {cals}
    }
    dump_cals(calNames){
        return Object.fromEntries(calNames.map(calName=>
            ([calName, this.cals[calName].dump()])
        ))

    }
    export(calName){
        return this.cals[calName].export()
    }
    exports(calNames){
        const export_all = !calNames

        const pack = tar.pack()
        Object.entries(this.cals)
        .filter(([calName,obj])=>
            export_all || calNames.includes(calName)
        ).forEach(([calName,obj])=>
            pack.entry({name: `calendar/${calName}.ics` }, obj.export())
        )
        pack.finalize()
        return pack
    }
    create_cal_if_not_exist(calName, calOpts){
        if(!this.cals.hasOwnProperty(calName))
            this.cals[calName] = new Calendar(calOpts)
    }
    async create_cals(calNames){
        calNames.forEach(calName=>this.create_cal_if_not_exist(calName))
    }
    async import(calName, importOpts){
        this.create_cal_if_not_exist(calName)
        await this.cals[calName].import(importOpts)
    }
    list(isTstrg){
        return Object.entries(this.cals).map(([calName,obj])=>{
            const metadata = JSON.parse(JSON.stringify(obj.metadata))
            if(isTstrg)
                metadata.lastUpdate = metadata.lastUpdate?
                    (new Date(metadata.lastUpdate)).toLocaleString('en-GB') : null
            return {calName, metadata}
        })
    }
    get_period({calNames, period, now, isTstrg}){
        if(!now)
            now = new Date(Date.now())
        else{
            switch(now){
                case 'tmr':
                    now = new Date(Date.now()+24*60*60*1000)
                    break
            }
            now = new Date(now)
        }

        var start = new Date(now.getTime())
        start.setHours(0,0,0,0)
        var end
        switch(period){
            default:
            case 'day':
                end = new Date(start.getTime())
                end.setDate(end.getDate()+1)
                break
            case 'week':
                start.setDate(start.getDate()-start.getDay() +1) //+1 monday
                end = new Date(start.getTime())
                end.setDate(start.getDate()+7)
                break
            case 'month':
                start.setDate(1)
                end = new Date(start.getTime())
                end.setMonth(end.getMonth()+1)
                break
            case 'all':
                start = -Infinity
                end = Infinity
                break
            case 'fut':
                end = Infinity
                break
        }

        if(start instanceof Date)
            start = start.getTime()
        if(end instanceof Date)
            end = end.getTime()

        if(calNames.length)
            return calNames.map(calName=>({
                calName, 
                events: this.cals[calName].get_interval({start, end, isTstrg})
            }))
        return Object.fromEntries(Object.entries(this.cals).map(([calName,obj])=>(
            [calName, obj.get_interval({start, end, isTstrg})]
        )))
    }
    delete_cals(calNames){
        calNames.forEach(calName=>
            delete this.cals[calName]
        )
    }
    delete_events(calName, indices){
        const cal = this.cals[calName]
        indices = indices.sort((a,b)=>(a-b)).reverse()
        indices = [... new Set(indices)]
        indices.forEach(i=>cal.delete_event(i))
        cal.finalize()
    }
    add_event(calName, event){
        const cal = this.cals[calName]
        this.create_cal_if_not_exist(calName)
        cal.add_event(event)
        cal.finalize()
    }
    patch_event(calName, index, event){
        const cal = this.cals[calName]
        cal.patch_event(index, event)
        cal.finalize()
    }
    async pull_url(calNames){
        const selectedCals = Object.keys(this.cals).filter(calName=>
            (!calNames.length || calNames.includes(calName))
            && this.cals[calName].metadata.hasOwnProperty('url')
        )

        const updated_cals = {}
        for(const calName of selectedCals){
            var oldCal = this.cals[calName]
            const url = oldCal.metadata.url
            const newCal = new Calendar({url})
            await newCal.import({url})
            const {add,del} = oldCal.diff(newCal)
            //import: http res is ok
            if(newCal.metadata.hasOwnProperty('lastUpdate')){
                newCal.finalize()
                updated_cals[calName] = {add,del}
                this.cals[calName] = newCal
            }
        }
        return updated_cals
    }
}

function has_intersect(start1,end1, start2,end2){
    return start1<=end2 && start2<=end1
}
class Calendar{
    constructor(opts={}){
        this.load(opts)
    }
    load(opts){
        const {events,metadata} = opts

        this.events = events || []
        this.metadata = metadata || {}
    }
    dump(){
        const {events,metadata} = this
        return {events,metadata}
    }
    standardize_event(e){//for storage
        if(!(e.start = (new Date(e.start)).getTime()))
            throw new Error('Bad date')
        e.end = (new Date(e.end)).getTime() || e.end || e.start
        if(e.start > e.end)
            e.end = e.start
    }
    humanize_events(events){//for response
        events.forEach(e=>{
            e.start = (new Date(e.start)).toLocaleString('en-GB')
            e.end = (new Date(e.end)).toLocaleString('en-GB')
        })
    }
    add_event({start,end,summary='',description=''}){
        var event = {start,end,summary,description}
        this.standardize_event(event)
        this.events.push(event)
        //this.finalize()
    }
    delete_event(index){
        this.events.splice(index,1)
        //this.finalize()
    }
    patch_event(index, newEvent){
        const event = this.events[index]
        Object.entries(newEvent)
        .filter(([k,v])=> v && event.hasOwnProperty(k))
        .forEach(([k,v])=> event[k] = v)
        this.standardize_event(event)
        this.finalize()
    }
    async import({ical, url, json}){
        if(json){
            this.load(Object.values(JSON.parse(json))[0])
            return
        }
        if(url){
            this.metadata.url = url
            const res = await fetch(url)
            if(res.ok){
                ical = await res.text()
                this.metadata.lastUpdate = Date.now()
            }
        }
        const events = icalParser.parseICS(ical)
        Object.entries(events).forEach(([key,e])=>{
            if(e.type == 'VEVENT'){
                const {start,end,summary,description} = e
                this.add_event({start,end,summary,description})
            }
        })
        this.finalize()
    }
    export(){
        const temp_ical = icalGenerator()
        this.events.forEach(e=>{
            e.start = new Date(e.start)
            e.end = new Date(e.end)
            temp_ical.createEvent(e)
        })
        return temp_ical.toString()
    }
    get_intersect(start,end){ //timestamp
        return this.events.map((e,index)=>(
            {index,...e}
        )).filter(e=> has_intersect(start,end,e.start,e.end))
    }
    get_interval({start, end, isTstrg}){ //timestamp
        const intersect = this.get_intersect(start,end)
        if(isTstrg)
            this.humanize_events(intersect)
        return intersect
    }
    finalize(){
        this.uniq()
        this.sort()
    }
    sort(){
        this.events.sort((a,b)=>a.start-b.start)
    }
    uniq(){
        this.events = [...new Set(this.events.map(JSON.stringify))].map(JSON.parse)
    }
    diff(newCal){
        const newEvents = newCal.events.map(JSON.stringify)
        const oldEvents = this.events.map(JSON.stringify)

        const add = newEvents.filter(e=>!oldEvents.includes(e)).map(JSON.parse)
        const del = oldEvents.filter(e=>!newEvents.includes(e)).map(JSON.parse)

        this.humanize_events(add)
        this.humanize_events(del)

        return {add,del}
    }
}

export {DB,CalendarHandler}